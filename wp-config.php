<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tuko');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4$c}!XQSByo0%l|N;-aY^mnb/=/5HyYd1NqUhsd$B2NR;?;I4_]QX.k,c~uPb+0P');
define('SECURE_AUTH_KEY',  'R+ZI6Ji7D?tVoc~gPs?%1tyPnF+0hm+Z*8 XY1pQ h9WtOw)Ao%gijP;=Su@viM<');
define('LOGGED_IN_KEY',    'k*H]p-)g1F&#qp#o2]Ev$JAy*UFK(Lv.S.D<=atF8HTSMl9PU]xL~96h`fL?7($-');
define('NONCE_KEY',        'd$wzyV!=Ng2larM(/6JNmabi{Z%$lyp>ucH4EkeZ$i}G|.^x^7mBo%FxT=H2<K{q');
define('AUTH_SALT',        '.R6@-odP?FX6[I(wUJz5R)T[U>[J;-J7qLi|)4DpE/gl9{!Jvb*3J`fv]N-YbS&=');
define('SECURE_AUTH_SALT', '`F#ZPt=6]xUV,{%n;0/[QS^fg,<]z!:Bx_6};-AkmzJI}fe0;1I?c(X!B@*NEHG.');
define('LOGGED_IN_SALT',   't6_E^Jiqyf=szczlvwiRn@GPE5Bci&]Hk_LN8)xtVM[)N&/senT<>F{cL.cE~S?C');
define('NONCE_SALT',       '@<>HkwqoE1@g t-?m)n-#g|p?C];2mlyoN^MYCaC1fW!tN)|3E!KO>.3)vs;$>W$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
