<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sparkle_Store
 */

?>	    
 <div id="footer" class="footer-page">
   
   <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section footer2 box-parallax vc_custom_1499747180168 vc_section-has-fill">
       <div class="container">
      <div class="vc_row wpb_row">
         <div class="wpb_column column_container col-sm-4">
            <div class="vc_column-inner ">
               <div class="wpb_wrapper">
                  <h2 class="mb-text-header wow   text-left title18 font-bold color  s7upf_1531443038 s7upf_1531443040">About Tuko Distribution Inc.</h2>
                  <div class="mb-text-header wow   text-left   s7upf_1531443041 s7upf_1531443042 s7upf_1531443044"><?php echo get_field('about_tuko_distribution',43); ?></div>
                  <div class="vc_row wpb_row vc_inner">
                     <div class="wpb_column column_container col-sm-12 col-has-fill">
                        <div class="vc_column-inner vc_custom_1493169129451">
                           <div class="wpb_wrapper">
                              <h2 class="mb-text-header wow   text-left title18 font-bold color  s7upf_1531443045 s7upf_1531443047">Online Payments by</h2>
                              <div class="payment-method text-left">
                                  <?php
                                    $payment = get_field('online_payments',43);
                                   
                                    foreach($payment as $item) {
                                  ?>
                                    <a href=""><img src="<?php echo $item['online_payments_image']; ?>"></a>
                                   
                                    <?php } ?>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="wpb_column column_container col-sm-4">
            <div class="vc_column-inner ">
               <div class="wpb_wrapper">
                  <h2 class="mb-text-header wow   text-left title18 font-bold color  s7upf_1531443048 s7upf_1531443050">Location Store</h2>
                  <div class="contact-footer2 s7upf_1531443053">
                     <?php
                        $payment = get_field('location_store',43); 
                            foreach($payment as $item) {
                        ?>
                        <p class="desc s7upf_1531443052">
                            <span class="color s7upf_1531443051"><?php echo $item['location_store_icon']; ?></span>
                            <?php echo $item['location_store_text']; ?>
                        </p>       
                    <?php } ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="wpb_column column_container col-sm-4">
            <div class="vc_column-inner ">
               <div class="wpb_wrapper">
                  <div class="footer-box2  s7upf_1531443054 mb-mailchimp   "  data-namesubmit = "SUBMIT" data-placeholder = "Enter your order number">
                     <h2 class="title18 font-bold color">Subscribe To Our Newsletter</h2>
                     <p class="desc">Stay tune for updates</p>
                     <div class="email-form2">
                        <i class="fa fa fa-envelope"></i>
                        <?php echo do_shortcode('[cp_simple_newsletter]' );?>
                     </div>
                  </div>
                  <div class="vc_row wpb_row vc_inner">
                     <div class="wpb_column column_container col-sm-12 col-has-fill">
                        <div class="vc_column-inner vc_custom_1493116023553">
                           <div class="wpb_wrapper">
                              <h2 class="mb-text-header wow   text-left title18 font-bold color  s7upf_1531443055 s7upf_1531443057">Connect With Us</h2>
                              <div class="social-network mb-element-social-small">
                              <?php
                                    $payment = get_field('social_networks',43); 
                                        foreach($payment as $item) {
                                    ?>
                                    <a class="float-shadow" href="<?php echo $item['social_networks_link']; ?>">
                                        <?php echo $item['social_networks_icon']; ?>
                                    </a>       
                                <?php } ?>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="vc_row wpb_row">
         <div class="wpb_column column_container col-sm-12">
            <div class="vc_column-inner ">
               <div class="wpb_wrapper">
                  <div class="wpb_text_column wpb_content_element  vc_custom_1493116559715" >
                     <div class="wpb_wrapper">
                        <div class="logo-footer2 text-center" style="max-width: 200px; margin: 0 auto;">
                                    <?php 
                            if ( function_exists( 'the_custom_logo' ) ) {
                                the_custom_logo();
                            } 
                        ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="vc_row wpb_row">
         <div class="wpb_column column_container col-sm-12">
            <div class="vc_column-inner ">
               <div class="wpb_wrapper">
                  <ul class="menu-footer2 list-inline-block  text-center s7upf_1531443064 s7upf_1531443065 s7upf_1531443066">
                  <?php
                        $productcat = get_terms('product_cat');
                        foreach($productcat as $item) {
                        $category_link = get_category_link($item->term_id);
                    ?>
                    <li><a href="<?php echo esc_url( $category_link ); ?>"><?php echo $item->name; ?></a></li>
                    <?php
                        }
                    ?>
                  </ul>
                  <div class="wpb_text_column wpb_content_element  vc_custom_1493116541970" >
                     <div class="wpb_wrapper">
                        <div class="bottom-footer2 text-center">
                           <p class="copyright2 desc white">Tuko Distributions Inc © 2018 eCommerce Store. All Rights Reserved.</p>
                           <p class="design2 desc white">Design by <a class="color" href="#">Blueinspires</a></p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>

</div>
</div><!-- #page -->

<a href="#" class="scroll-top round"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>

<?php wp_footer(); ?>
<script type="text/javascript">
         (function($) {
             "use strict";
             $("head").append('<style id="sv_add_footer_css">.vc_custom_1493280748633{padding-bottom: 30px !important;}.vc_custom_1493280374466{margin-bottom: 0px !important;} .s7upf_1531442955 li a{ color:; } .s7upf_1531442956 li a:hover{ color:; } .s7upf_1531442957 li a{ text-transform:none; } .s7upf_1531442958 li a{ color:; } .s7upf_1531442959 li a:hover{ color:; } .s7upf_1531442960 li a{ text-transform:none; } .s7upf_1531442961 li a{ color:#555555; } .s7upf_1531442962 li a:hover{ color:#a0b31b; } .s7upf_1531442963 li a{ text-transform:none; } .vc_custom_1493972191411{margin-bottom: 20px !important;}.vc_custom_1493873140508{margin-top: 20px !important;margin-bottom: 0px !important;}.vc_custom_1529287027947{margin-bottom: 0px !important;} .s7upf_1531442964{ color:; } .s7upf_1531442965{ color:; } .s7upf_1531442966{ color:; } .s7upf_1531442967.btn-arrow{ border-color:; } .s7upf_1531442968.btn-arrow:hover{ background:; } .s7upf_1531442969.btn-arrow:after{ background:; } .s7upf_1531442970.btn-arrow.style2{ background:; } .s7upf_1531442971.btn-arrow.style2:hover{ background:; } .s7upf_1531442972 .item-fruit-cat1::before{ background-image : url("wp-content/uploads/2017/04/menu0.png") } .s7upf_1531442973 .item-fruit-cat1::before{ background-image : url("wp-content/uploads/2017/04/menu0.png") } .s7upf_1531442974 .item-fruit-cat1::before{ background-image : url("wp-content/uploads/2017/04/menu0.png") } .s7upf_1531442975 .product-in-cat a:hover{ color:; } .s7upf_1531442976 .title-tab-icon li a{ border-color:; } .s7upf_1531442977 .title-tab-icon li.active a{ background-color:; } .s7upf_1531442978 .title-tab-icon li a:hover{ background-color:; } .s7upf_1531442979 .title-tab1 li a:hover{ color:; } .s7upf_1531442980 .title-tab1 li.active a{ background-color:; border-color:; color:#fff; } .s7upf_1531442981 .btn-arrow.color{ border-color:; color:; } .s7upf_1531442982 .btn-arrow.color:hover{ background-color:; color:#fff; } .s7upf_1531442983 .title-tab-layout2 li::after{ background-color:; } .s7upf_1531442984 .product-title .color{ color:; } .s7upf_1531442985 .title-tab-icon3 a:hover{ color:; } .s7upf_1531442986 .title-tab-icon3 li.active a{ color:; } .s7upf_1531442987 .color{ color:; } .s7upf_1531442988 .days-countdown .time_circles > div{ color:; } .s7upf_1531442989 .days-countdown .time_circles > div .text{ color:; } .s7upf_1531442990{ background:rgba(255,255,255,0.01); } .s7upf_1531442991 .days-countdown .time_circles > div .text::before{ background:; } .s7upf_1531442992 .sub_title{ color:#ffffff!important; } .s7upf_1531442993{ text-transform:none!important; } .s7upf_1531442994 .service-icon a{ color:; border-color:; } .s7upf_1531442995 .service-icon a::before{ border-color:; } .s7upf_1531442996 .desc{ color:#ffffff; } .s7upf_1531442997{ color:#ffffff; } .s7upf_1531442998:hover{ color:; } .s7upf_1531442999 .service-icon a{ color:; border-color:; } .s7upf_1531443000 .service-icon a::before{ border-color:; } .s7upf_1531443001 .desc{ color:#ffffff; } .s7upf_1531443002{ color:#ffffff; } .s7upf_1531443003:hover{ color:; } .s7upf_1531443004 .service-icon a{ color:; border-color:; } .s7upf_1531443005 .service-icon a::before{ border-color:; } .s7upf_1531443006 .desc{ color:#ffffff; } .s7upf_1531443007{ color:#ffffff; } .s7upf_1531443008:hover{ color:; } .s7upf_1531443009 .service-icon a{ color:; border-color:; } .s7upf_1531443010 .service-icon a::before{ border-color:; } .s7upf_1531443011 .desc{ color:#ffffff; } .s7upf_1531443012{ color:#ffffff; } .s7upf_1531443013:hover{ color:; } .s7upf_1531443014{ text-transform:none!important; } .s7upf_1531443015 .sub_title{ color:#ffffff!important; } .s7upf_1531443016{ text-transform:none!important; } .s7upf_1531443017{ color: ; } .s7upf_1531443018{ color: #ffffff; } .s7upf_1531443019{ color: #ffffff; } .s7upf_1531443020 .desc::before{ background: ; } .s7upf_1531443021 .client-thumb a{ border-color: ; } .s7upf_1531443022 .client-thumb a img{ border-color: ; } .s7upf_1531443023 .owl-theme .owl-controls .owl-buttons div:hover{ background: ; border-color:; } .s7upf_1531443024{ text-transform:none!important; } .s7upf_1531443025{ color: ; } .s7upf_1531443026 .title-underline::after{ background: ; } .s7upf_1531443027 .btn-arrow{ color: ; border-color:; } .s7upf_1531443028{ color: ; } .s7upf_1531443029{ color: ; } .s7upf_1531443030 .banner-info .info-product-adv3{ background: ; } .s7upf_1531443031.btn-arrow{ color: ; border-color: ; } .s7upf_1531443032.btn-arrow:after{ background: ; } .s7upf_1531443033.btn-arrow:hover{ background: ; } .s7upf_1531443034 .ver.banner-info{ height: px; } .s7upf_1531443035{ text-transform:none!important; } .s7upf_1531443036{ text-transform:none!important; } .s7upf_1531443037 a img{ width:150px; height:150px; } .s7upf_1531443038{ margin-bottom:18px!important; } .s7upf_1531443039{ margin-bottom:18px!important; } .s7upf_1531443040{ text-transform:none!important; } .s7upf_1531443041{ line-height:24px!important; } .s7upf_1531443042{ color:#ffffff!important; } .s7upf_1531443043 .color{ color:#ffffff!important; } .s7upf_1531443044{ text-transform:none!important; } .s7upf_1531443045{ margin-bottom:25px!important; } .s7upf_1531443046{ margin-bottom:25px!important; } .s7upf_1531443047{ text-transform:none!important; } .s7upf_1531443048{ margin-bottom:18px!important; } .s7upf_1531443049{ margin-bottom:18px!important; } .s7upf_1531443050{ text-transform:none!important; } .s7upf_1531443051{ color: #45bf61; } .s7upf_1531443052{ color: #ffffff; } .s7upf_1531443053 .desc span{ background: ; } .s7upf_1531443054 .desc{ color:#ffffff; } .s7upf_1531443055{ margin-bottom:25px!important; } .s7upf_1531443056{ margin-bottom:25px!important; } .s7upf_1531443057{ text-transform:none!important; } .s7upf_1531443058{ color:; } .s7upf_1531443059{ background:#3c5998!important; } .s7upf_1531443060{ background:#3eb1e2!important; } .s7upf_1531443061{ background:#ff2323!important; } .s7upf_1531443062{ background:#ff6c00!important; } .s7upf_1531443063{ background:#0f537a!important; } .s7upf_1531443064 li a{ color:#ffffff; } .s7upf_1531443065 li a:hover{ color:#45bf61; } .s7upf_1531443066 li a{ text-transform:none; } .vc_custom_1499747180168{padding-top: 60px !important;padding-bottom: 30px !important;background-image: url(http://7uptheme.com/wordpress/fruitshop/wp-content/uploads/2017/04/prl3.jpg?id=342) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1493169129451{margin-top: 10px !important;border-top-width: 1px !important;padding-top: 25px !important;border-top-color: rgba(255,255,255,0.2) !important;border-top-style: solid !important;}.vc_custom_1493116023553{margin-top: 30px !important;border-top-width: 1px !important;padding-top: 25px !important;border-top-color: rgba(255,255,255,0.2) !important;border-top-style: solid !important;}.vc_custom_1493116559715{margin-bottom: 20px !important;}.vc_custom_1493116541970{margin-bottom: 0px !important;} .s7upf_1531443067{ background: url(); background-size: 100% 100%; }</style>');
         })(jQuery);
      </script>



      <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/fruitshop/assets/js/lib/bootstrap.min.js'></script>
      <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/fruitshop/assets/js/lib/owl.carousel.min.js'></script>
      <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/fruitshop/assets/js/lib/jquery.hoverdir.js'></script>
      <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/fruitshop/assets/js/theme.js'></script>

</body>
</html>