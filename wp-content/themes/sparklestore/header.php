<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sparkle_Store
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> <?php sparklestore_html_tag_schema(); ?> >
<head>
    <title><?php the_title(); ?></title>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php wp_head(); ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/assets/css/custom.css'; ?>">
    <link rel="stylesheet" id="bootstrap-css"  href="<?php echo get_template_directory_uri();?>/fruitshop/assets/css/lib/bootstrap.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="font-awesome.min-css"  href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="owl-carousel-css"  href="<?php echo get_template_directory_uri();?>/fruitshop/assets/css/lib/owl.carousel.css" type="text/css" media="all" />
    <link rel="stylesheet" id="owl-transitions-css"  href="<?php echo get_template_directory_uri();?>/fruitshop/assets/css/lib/owl.transitions.css" type="text/css" media="all" />
    <link rel="stylesheet" id="owl-theme-css"  href="<?php echo get_template_directory_uri();?>/fruitshop/assets/css/lib/owl.theme.css" type="text/css" media="all" />
    <link rel="stylesheet" id="s7upf-color-css"  href="<?php echo get_template_directory_uri();?>/fruitshop/assets/css/color.css" type="text/css" media="all" />
    <link rel="stylesheet" id="s7upf-theme-css"  href="<?php echo get_template_directory_uri();?>/fruitshop/assets/css/theme.css" type="text/css" media="all" />
    <link rel="stylesheet" id="s7upf-responsive-css"  href="<?php echo get_template_directory_uri();?>/fruitshop/assets/css/responsive.css" type="text/css" media="all" />
    <link rel="stylesheet" id="s7upf-mb-style-css"  href="<?php echo get_template_directory_uri();?>/fruitshop/assets/css/style-mb.css" type="text/css" media="all" />
    <link rel="stylesheet" id="s7upf-theme-default-css"  href="<?php echo get_template_directory_uri();?>/fruitshop/style.css" type="text/css" media="all" />
</head>

<body <?php body_class(); ?>>

    <div id="page" class="site">

        <?php
            /**
             * @see  sparklestore_skip_links() - 5
             */
            do_action('sparklestore_header_before');

            /**
             * @see  sparklestore_top_header() - 15
             * @see  sparklestore_main_header() - 20
             */
            do_action('sparklestore_header');

            do_action('sparklestore_header_after');

        ?>