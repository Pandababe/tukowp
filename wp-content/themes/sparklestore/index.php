<?php
/**
	* Template Name: Homepage
 */

get_header(); ?>

<section>
<div class="mainbanner-slider">
	<ul class="slides">
		<?php
			$mainbanner = get_field('field_5bcac393aa8b5');
			foreach($mainbanner as $item) {
		?>
		<li class="item">
            <img src="<?php echo $item['main_banner_image']; ?>" alt="">
		</li>
		<?php
			}
		?>
	</ul>
</div>
</section>
<section class="sectionarea-02 row">

	<div class="col-sm-6 mainbanner-slider-left">
        <ul class="slides">
            <?php
                $mainbannerleft = get_field('field_5bcac3c9aa8b7');
                foreach($mainbannerleft as $item) {
            ?>
            <li class="item">
                <img src="<?php echo $item['main_banner_left_image']; ?>" alt="">
            </li>
            <?php
                }
            ?>
        </ul>
	</div>
	<div class="col-sm-6 mainbanner-slider-right">	
        <ul class="slides">
            <?php
                $mainbannerright = get_field('field_5bcac402aa8b9');
                foreach($mainbannerright as $item) {
            ?>
            <li class="item">
                <img src="<?php echo $item['main_banner_right_image']; ?>" alt="">
            </li>
            <?php
                }
            ?>
        </ul>
	</div>

</section>
<section class="sectionarea-03">
	<div class="container">
		<h2>TRENDING SEARCH</h2>
		<div class="col-md-4">
			<a href=""><img src="<?php echo get_field('image01'); ?>" alt="" class="img-responsive"></a></div>
		<div class="col-md-4">
			<a href=""><img src="<?php echo get_field('image02'); ?>" alt="" class="img-responsive"></a>
			<h3>GUARANTEED FRESH<span>Our latest Tuko Fresh Produce</span></h3>
		</div>
		<div class="col-md-4">
			<a href=""><img src="<?php echo get_field('image03'); ?>" alt="" class="img-responsive"></a>
		</div>
	</div>
</section>

<div class="wrap preload">
         <section id="content" class="main-wrapper">
            <div class="bg-green">
                <div class="container" style="margin-bottom: 0;">        
                    <div class="vc_row-full-width vc_clearfix"></div>
                    <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_custom_1493954553190 vc_row-has-fill">
                    <div class="wpb_column column_container col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="tab-content">

                                    <div id="tab-vip15b47f70b48f59" class="tab-pane active">
                                        <div class="product-slider">
                                        <div class="wrap-item" data-navigation="true" data-pagination="false" data-autoplay="true" data-itemscustom="[[0,1],[560,2],[768,3],[990,4]]">
                                                <?php
                                                    $the_query = new WP_Query( array(
                                                        'post_type' => 'product'
                                                    ) ); 
                                                    if ( $the_query->have_posts() ) {
                                                        while ( $the_query->have_posts() ) {
                                                            $the_query->the_post();
                                                                $_product = wc_get_product( get_the_ID() );
                                                    
                                                            
                                                        ?>
                                                    <div class="item-product  text-center product">
                                                        <div class="product-thumb">
                                                            <a href="<?php echo get_permalink(); ?>" class="product-thumb-link rotate-thumb ">
                                                            <img width="300" height="300" src="<?php echo get_the_post_thumbnail_url(); ?>" class="attachment-300x300 size-300x300 wp-post-image" alt="" />
                                                            <img width="300" height="300" src="<?php echo get_the_post_thumbnail_url(); ?>" class="attachment-300x300 size-300x300" alt=""  />       
                                                            </a>
                                                            <a href="<?php echo get_permalink(); ?>" class="quickview-link product-ajax-popup"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                        </div>
                                                        <div class="product-info">
                                                            <h3 class="product-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                            <div class="product-price">
                                                                <?php if($_product->get_sale_price()) { ?>
                                                                    <del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_regular_price(); ?></span></del>
                                                                    <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_sale_price(); ?> </span>per KG</span>
                                                                <?php } else { ?>
                                                                <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_regular_price(); ?> </span>per KG</span>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="product-rate ">
                                                                <div class="product-rating" style="width:<?php echo ($_product->get_average_rating()/5)*100 .'%'; ?>"></div>
                                                            </div>
                                                            <div class="product-extra-link">
                                                                <a href="?add_to_wishlist=<?php echo get_the_ID(); ?>" class="mb-wishlist add_to_wishlist wishlist-link" rel="nofollow" data-product-id="<?php echo get_the_ID(); ?>" data-product-title="<?php the_title(); ?>">
                                                                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                                                                </a>
                                                                <a href="?add-to-cart=<?php echo get_the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php echo get_the_ID(); ?>" data-product_sku="<?php echo $_product->get_sku(); ?>" aria-label="Add &ldquo;<?php the_title(); ?>&rdquo; to your cart" rel="nofollow">Add to cart</a> 
                                                            </div>
                                                            <div class="product-in-cat list-inline-block">
                                                                <?php
                                                                
                                                                $terms = get_the_terms(  get_the_ID() , 'product_tag' );
                                                                if($terms) {
                                                                    echo '/';
                                                                    foreach ( $terms as $term ) { ?>
                                                                    <a href="<?php echo site_url(). '/product-tag/'. $term->slug; ?>" rel="tag"><?php echo $term->name; ?></a> /  
                                                                <?php
                                                                    } 
                                                                } else {
                                                                    echo "&nbsp;";
                                                                }
                                                                ?>                                       
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                                    
                                                        }
                                                        wp_reset_postdata();
                                                } else {
                                                    echo '<p>No Product Found</p>';
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>         
            </div>
            <div class="bg-green full">
            <div class="heading-tagline"><?php the_field('coupon_sentence'); ?><span><?php the_field('coupon_code'); ?></span></div>
            <!-- Order now and you'll get FREE TUKO FRESH PRODUCE SHOPPING BAG use Coupon Code: -->
                <div class="container">
                <div class="vc_row-full-width vc_clearfix"></div>
                    <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_custom_1493954553190 vc_row-has-fill">
                        <div class="wpb_column column_container col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="tab-content"> 
                                    <h1 class="heading-center">Todays Deal</h1>
                                        <div id="tab-vip15b47f70b48f59" class="tab-pane active">
                                            <div class="product-slider">
                                            <div class="wrap-item" data-navigation="true" data-pagination="false" data-autoplay="true" data-itemscustom="[[0,1],[560,2],[768,3],[990,4]]">
                                                    <?php
                                                        $the_query = new WP_Query( array(
                                                            'product_tag' => 'todays_deal'
                                                        ) ); 
                                                        if ( $the_query->have_posts() ) {
                                                            while ( $the_query->have_posts() ) {
                                                                $the_query->the_post();
                                                                $_product = wc_get_product( get_the_ID() );
                                                        ?>
                                                        <div class="item-product  text-center product">
                                                            <?php if(get_field('tagline')) { ?>
                                                                <span class="prod-tagline"><?php echo get_field('tagline'); ?></span>
                                                            <?php } ?>
                                                            
                                                            <div class="product-thumb">
                                                                <a href="<?php echo get_permalink(); ?>" class="product-thumb-link rotate-thumb ">
                                                                <img width="300" height="300" src="<?php echo get_the_post_thumbnail_url(); ?>" class="attachment-300x300 size-300x300 wp-post-image" alt="" />
                                                                <img width="300" height="300" src="<?php echo get_the_post_thumbnail_url(); ?>" class="attachment-300x300 size-300x300" alt=""  />       
                                                                </a>
                                                                <a href="<?php echo get_permalink(); ?>" class="quickview-link product-ajax-popup"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                            </div>
                                                            <div class="product-info">
                                                                <h3 class="product-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                                <div class="product-price">
                                                                    <?php if($_product->get_sale_price()) { ?>
                                                                        <del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_regular_price(); ?></span></del>
                                                                        <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_sale_price(); ?> </span>per KG</span>
                                                                    <?php } else { ?>
                                                                    <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_regular_price(); ?> </span>per KG</span>
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="product-rate ">
                                                                    <div class="product-rating" style="width:<?php echo ($_product->get_average_rating()/5)*100 .'%'; ?>"></div>
                                                                </div>
                                                                <div class="product-extra-link">
                                                                    <a href="?add_to_wishlist=<?php echo get_the_ID(); ?>" class="mb-wishlist add_to_wishlist wishlist-link" rel="nofollow" data-product-id="<?php echo get_the_ID(); ?>" data-product-title="<?php the_title(); ?>">
                                                                        <i class="fa fa-heart-o" aria-hidden="true"></i>
                                                                    </a>
                                                                    <a href="?add-to-cart=<?php echo get_the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php echo get_the_ID(); ?>" data-product_sku="<?php echo $_product->get_sku(); ?>" aria-label="Add &ldquo;<?php the_title(); ?>&rdquo; to your cart" rel="nofollow">Add to cart</a> 
                                                                </div>
                                                                <div class="product-in-cat list-inline-block">
                                                                    <div class="sold"><p>
                                                                        <?php if(get_field('sold')) {
                                                                            echo get_field('sold');
                                                                        } else {
                                                                            echo '0';
                                                                        } ?>
                                                                    Sold</p></div>           
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php
                                                            }
                                                            wp_reset_postdata();
                                                    } else {
                                                        echo '<p>No Product Found</p>';
                                                    } ?>
                                            </div>
                                            </div>
                                        </div>

                                    </div>

                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">           
                <div class="vc_row-full-width vc_clearfix"></div>
                <div class="vc_row wpb_row vc_custom_1493113377249">
                    <div class="wpb_column column_container col-sm-4">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class=" box-product-type mb-element-product-style4 woocommerce  ">
                                    <h2 class="color2 title30 text-left title-box2">Special</h2>
                                    <div class="product-type-slider">
                                        <div class="wrap-item group-navi" data-pagination="false" data-navigation="true"  data-autoplay="false" data-itemscustom="[[0,1],[560,2],[768,1]]">

                                                <?php
                                                    $the_query = new WP_Query( array(
                                                        'product_tag'       => 'special'
                                                    ) ); 
                                                
                                                    if ( $the_query->have_posts() ) {
                                                        while ( $the_query->have_posts() ) {
                                                            $the_query->the_post();
                                                            $_product = wc_get_product( get_the_ID() );
                                                ?>
                                                
                                                        <div class="item-product-type table product">
                                                            <div class="product-thumb">
                                                                <a href="<?php echo get_permalink(); ?>" class="product-thumb-link zoom-thumb ">
                                                                    <img width="600" height="600" srcset="<?php echo get_the_post_thumbnail_url(); ?>" class="attachment-600x600 size-600x600 wp-post-image" alt="" sizes="(max-width: 600px) 100vw, 600px" />
                                                                </a>
                                                                    <a data-product-id="<?php echo get_the_ID(); ?>" href="<?php echo get_permalink(); ?>" class="quickview-link product-ajax-popup"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                            </div>
                                                            <div class="product-info">
                                                                <div class="product-price">
                                                                    <?php if($_product->get_sale_price()) { ?>
                                                                        <del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_regular_price(); ?></span></del>
                                                                        <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_sale_price(); ?> </span>per KG</span>
                                                                    <?php } else { ?>
                                                                        <del><span class="woocommerce-Price-amount amount">&nbsp;</span></del>
                                                                    <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_regular_price(); ?> </span>per KG</span>
                                                                    <?php } ?>
                                                                </div>
                                                                <h3 class="product-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                                                    
                                                                <div class="product-rate ">
                                                                    <div class="product-rating" style="width:<?php echo ($_product->get_average_rating()/5)*100 .'%'; ?>"></div>
                                                                </div>
                                                                <a href="?add-to-cart=<?php echo get_the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php echo get_the_ID(); ?>" data-product_sku="<?php echo $_product->get_sku(); ?>" aria-label="Add &ldquo;<?php the_title(); ?>&rdquo; to your cart" rel="nofollow">Add to cart</a>
                                                            </div>
                                                        </div>
                                                <?php      
                                                        }
                                                        wp_reset_postdata();
                                                } else {
                                                    echo '<p>No Post Found!</p>';
                                                } ?>
                                        </div>
                                    </div>
                                    <div class="view-all-product text-center">
                                        <a  href="<?php echo site_url(). '/product-tag/special'; ?>"  target="_parent" class="btn-arrow color style2">View All</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column column_container col-sm-4">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                            <div class=" box-product-type mb-element-product-style4 woocommerce  ">
                                <h2 class="color2 title30 text-left title-box2">Popular</h2>
                                <div class="product-type-slider">
                                    <div class="wrap-item group-navi" data-pagination="false" data-navigation="true"  data-autoplay="false" data-itemscustom="[[0,1],[560,2],[768,1]]">

                                        <?php
                                            $the_query = new WP_Query( array(
                                                'product_tag'       => 'popular'
                                            ) ); 
                                            
                                            if ( $the_query->have_posts() ) {
                                                while ( $the_query->have_posts() ) {
                                                    $the_query->the_post();
                                                        $_product = wc_get_product( get_the_ID() );
                                        ?>
                                            
                                                <div class="item-product-type table product">
                                                    <div class="product-thumb">
                                                        <a href="<?php echo get_permalink(); ?>" class="product-thumb-link zoom-thumb ">
                                                            <img width="600" height="600" srcset="<?php echo get_the_post_thumbnail_url(); ?>" class="attachment-600x600 size-600x600 wp-post-image" alt="" sizes="(max-width: 600px) 100vw, 600px" />
                                                        </a>
                                                            <a data-product-id="<?php echo get_the_ID(); ?>" href="<?php echo get_permalink(); ?>" class="quickview-link product-ajax-popup"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                    </div>
                                                    <div class="product-info">
                                                        <div class="product-price">
                                                        <?php if($_product->get_sale_price()) { ?>
                                                            <del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_regular_price(); ?></span></del>
                                                            <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_sale_price(); ?> </span>per KG</span>
                                                        <?php } else { ?>
                                                            <del><span class="woocommerce-Price-amount amount">&nbsp;</span></del>
                                                        <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_regular_price(); ?> </span>per KG</span>
                                                        <?php } ?>
                                                        </div>
                                                        <h3 class="product-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                                            
                                                        <div class="product-rate ">
                                                            <div class="product-rating" style="width:<?php echo ($_product->get_average_rating()/5)*100 .'%'; ?>"></div>
                                                        </div>
                                                        <a href="?add-to-cart=<?php echo get_the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php echo get_the_ID(); ?>" data-product_sku="<?php echo $_product->get_sku(); ?>" aria-label="Add &ldquo;<?php the_title(); ?>&rdquo; to your cart" rel="nofollow">Add to cart</a>
                                                    </div>
                                                </div>
                                        <?php      
                                                }
                                                wp_reset_postdata();
                                        } else {
                                            echo '<p>No Post Found!</p>';
                                        } ?>
                                    </div>
                                </div>
                                <div class="view-all-product text-center">
                                    <a  href="<?php echo site_url(). '/product-tag/popular'; ?>"  target="_parent" class="btn-arrow color style2">View All</a>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column column_container col-sm-4">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                            <div class=" box-product-type mb-element-product-style4 woocommerce  ">
                                <h2 class="color2 title30 text-left title-box2">Recent</h2>
                                <div class="product-type-slider">
                                    <div class="wrap-item group-navi" data-pagination="false" data-navigation="true"  data-autoplay="false" data-itemscustom="[[0,1],[560,2],[768,1]]">
                                        <?php
                                            $the_query = new WP_Query( array(
                                                'product_tag'       => 'recent'
                                            ) ); 
                                            
                                            if ( $the_query->have_posts() ) {
                                                while ( $the_query->have_posts() ) {
                                                    $the_query->the_post();
                                                        $_product = wc_get_product( get_the_ID() );
                                        ?>
                                            
                                                <div class="item-product-type table product">
                                                    <div class="product-thumb">
                                                        <a href="<?php echo get_permalink(); ?>" class="product-thumb-link zoom-thumb ">
                                                            <img width="600" height="600" srcset="<?php echo get_the_post_thumbnail_url(); ?>" class="attachment-600x600 size-600x600 wp-post-image" alt="" sizes="(max-width: 600px) 100vw, 600px" />
                                                        </a>
                                                            <a data-product-id="<?php echo get_the_ID(); ?>" href="<?php echo get_permalink(); ?>" class="quickview-link product-ajax-popup"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                    </div>
                                                    <div class="product-info">
                                                        <div class="product-price">
                                                        <?php if($_product->get_sale_price()) { ?>
                                                            <del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_regular_price(); ?></span></del>
                                                            <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_sale_price(); ?> </span>per KG</span>
                                                        <?php } else { ?>
                                                            <del><span class="woocommerce-Price-amount amount">&nbsp;</span></del>
                                                        <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₱</span><?php echo $_product->get_regular_price(); ?> </span>per KG</span>
                                                        <?php } ?>
                                                        </div>
                                                        <h3 class="product-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                                            
                                                        <div class="product-rate ">
                                                            <div class="product-rating" style="width:<?php echo ($_product->get_average_rating()/5)*100 .'%'; ?> "></div>
                                                        </div>
                                                        <a href="?add-to-cart=<?php echo get_the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php echo get_the_ID(); ?>" data-product_sku="<?php echo $_product->get_sku(); ?>" aria-label="Add &ldquo;<?php the_title(); ?>&rdquo; to your cart" rel="nofollow">Add to cart</a>
                                                    </div>
                                                </div>
                                        
                                        <?php
                                
                                                }
                                                wp_reset_postdata();
                                        } else {
                                            echo '<p>No Post Found!</p>';
                                        } ?>
                                    </div>
                                </div>
                                <div class="view-all-product text-center">
                                    <a  href="<?php echo site_url(). '/product-tag/recent'; ?>"  target="_parent" class="btn-arrow color style2">View All</a>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                                  
            <!--End .container-->
         </section>
         
         <!-- End .main-wrapper-->
        

<?php get_footer();