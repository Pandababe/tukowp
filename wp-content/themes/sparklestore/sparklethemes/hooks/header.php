<?php
/**
 * Header Section Skip Area
*/
if ( ! function_exists( 'sparklestore_skip_links' ) ) {
	/**
	 * Skip links
	 * @since  1.0.0
	 * @return void
	 */
	function sparklestore_skip_links() { ?>
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'sparklestore' ); ?></a>
		<?php
	}
}
add_action( 'sparklestore_header_before', 'sparklestore_skip_links', 5 );


if ( ! function_exists( 'sparklestore_header_before' ) ) {
	/**
	 * Header Area
	 * @since  1.0.0
	 * @return void
	*/
	function sparklestore_header_before() { ?>
		<header id="masthead" class="site-header" itemscope="itemscope" itemtype="http://schema.org/WPHeader" role="banner">		
			<div class="header-container">
		<?php
	}
}
add_action( 'sparklestore_header_before', 'sparklestore_header_before', 10 );

/**
 * Top Header Area
*/
if ( ! function_exists( 'sparklestore_top_header' ) ) {
	
	function sparklestore_top_header() { ?>
	    <div class="topheader">
	        <div class="container">
                <div class="row">
		        	<div class="quickinfowrap">			            
						<?php wp_nav_menu( array( 'theme_location' => 'sparkletopleftmenu', 'depth' => 1) ); ?>
		        	</div>

		          	<div class="toplinkswrap">
			            <div class="toplinks">
							
							  <?php
							  if( is_user_logged_in() ) {
								wp_nav_menu( array( 'theme_location' => 'sparkletopmenulogin' ) );
								} else {
									wp_nav_menu( array( 'theme_location' => 'sparkletopmenulogout' ) );
								}
							  ?>
			            </div><!-- End Header Top Links --> 
		          	</div>
                </div>
	        </div>
	    </div>
		<?php
	}
}
add_action( 'sparklestore_header', 'sparklestore_top_header', 15 );


/**
 * Main Header Area
*/
if ( ! function_exists( 'sparklestore_main_header' ) ) {
	
	function sparklestore_main_header() { ?>
		<div class="mainheader">
			<div class="container sp-clearfix">
				<div class="col-sm-3 col-xs-6">
					<div class="sparkle-logo">
						<?php 
							if ( function_exists( 'the_custom_logo' ) ) {
								the_custom_logo();
							} 
						?>
					</div>
				</div>
				<div class="col-sm-2 col-xs-6 sp-mid col-sm-push-7">
					<div class="lib">
						<?php if ( sparklestore_is_woocommerce_activated() ) {  ?>
							<div class="view-cart">
								<?php echo sparklestore_shopping_cart(); ?>
								<div class="top-cart-content">
									<div class="block-subtitle"><?php esc_html_e('Recently added item(s)', 'sparklestore'); ?></div>
									<?php the_widget('WC_Widget_Cart', 'title='); ?>
								</div>
							</div>
						<?php } ?>	
					</div>
					<div class="lib">
						<div class="wishlist">
							<img src="<?php echo get_template_directory_uri(). '/assets/images/fast-shipping.jpg'; ?>" alt="">
						</div>
					</div>
				</div> 
				<div class="col-sm-7 col-xs-12 col-sm-pull-2">		
					<div class="rightheaderwrap sp-clearfix">
						<div class="category-search-form">
							<h1 class="head-tagline">A <span>marketplace</span> for locally, naturally farmed foods</h1>
							<?php 
								if ( sparklestore_is_woocommerce_activated() ) {  
									sparklestore_advance_search_form(); 
								} 
							?>
							<ul class="little-nav clearfix">
								<?php
									$productcat = get_terms('product_cat');
									foreach($productcat as $item) {
									$category_link = get_category_link($item->term_id);
								?>
								<li><a href="<?php echo esc_url( $category_link ); ?>"><?php echo $item->name; ?></a></li>
								<?php
									}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>		    
		<?php
	}
}
add_action( 'sparklestore_header', 'sparklestore_main_header', 20 );


if ( ! function_exists( 'sparklestore_header_after' ) ) {
	/**
	 * Header Area
	 * @since  1.0.0
	 * @return void
	*/
	function sparklestore_header_after() {
		?>
			</div>
		</header><!-- #masthead -->
		<?php
	}
}
add_action( 'sparklestore_header_after', 'sparklestore_header_after', 25 );